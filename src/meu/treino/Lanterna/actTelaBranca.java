package meu.treino.Lanterna;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.WindowManager;

public class actTelaBranca extends Activity {
	PowerManager pm;
	PowerManager.WakeLock wl;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.tela_branca);

		/**Alterando o Brilho da tela para o máximo 1 (de 0,1 a 1)********************/
		WindowManager.LayoutParams layoutParametros = getWindow().getAttributes();
		layoutParametros.screenBrightness = 1.0f;
		getWindow().setAttributes(layoutParametros);
		/*****************************************************************************/

		/**MANTENDO A TELA ACESA. PROIBE QUE A TELA SE APAGUE OU TRAVE****************/
		pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wl = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "Lanterna");

		/******************************************************************************/
	}

	@Override
	protected void onResume(){

		super.onResume();

		this.wl.acquire();
	}

	@Override
	protected void onStop() {

		super.onStop();

		this.wl.release();
	}
}