package meu.treino.Lanterna;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import java.io.IOException;


public class actLanterna extends Activity implements SurfaceHolder.Callback {
    /**
	 *	DECLARAÇÃO DAS VARIÁVEIS *******************************************************************
     */
    public static SurfaceView mSurface;
	public static SurfaceHolder mHolder;

	private Camera mCamera;
	Camera.Parameters params;


	private boolean isFlashOn = false;
	private boolean temFlash = false;
	private boolean isCameraOn = false;
	private boolean temCamera = false;

	private static final String TAG = "Lanterna";

	private	DroidLED led = null;

	private boolean acesa = false;
	private boolean som = false;

	MediaPlayer mpLiga, mpDesliga;

	/**********************************************************************************************/

	/**********************************************************************************************/
	/** IMPLEMENTAÇÃO DOS MÉTODOS(EVENTOS) DE LIFECYCLE DA ACTIVITY */
	@Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // Cria o layout do activity com base no layout main.xml***************************
        // é aqui que é possível criar outros layouts, como, por exemplo um layout para
        // o celular em retrato e outro para o celular em paisagem.
        setContentView(R.layout.main);
		//*********************************************************************************

		//GARANTINDO QUE O FUNDO DE TELA SEJA PRETO****************************************
		View view = this.getWindow().getDecorView();
		view.setBackgroundColor(Color.BLACK);
		//*********************************************************************************

		//VERIFICANDO AS CONFIGURAÇÕES DO SOM**********************************************
		SharedPreferences config = getPreferences(MODE_PRIVATE);
		som = config.getBoolean("som", true);
		//-------
		ImageView ivSom = (ImageView) findViewById(R.id.ivSom);
		if (som) {
			ivSom.setImageDrawable(getResources().getDrawable(R.drawable.soundon96x96));
		} else {
			ivSom.setImageDrawable(getResources().getDrawable(R.drawable.soundoff96x96));
		}
		//**********************************************************************************

		//INICIALIZANDO O TOCADOR DE SOM****************************************************
		mpLiga = MediaPlayer.create(this, R.raw.file5);
		mpDesliga = MediaPlayer.create(this, R.raw.file2);
		//**********************************************************************************

		//VERIFICANDO SE É UM MOTOROLA COM FLASH********************************************
		if (getMarca().equalsIgnoreCase("motorola")) {
			this.temFlash = this.temFlash();

			if (this.temFlash) {
				try {
					led = new DroidLED();
				} catch (Exception e) {
					Toast.makeText(this, "Deu pau ao instanciar LED Motorola.", Toast.LENGTH_SHORT).show();
				}
			}

		} else {
			this.prepara();
		}
		//**********************************************************************************

		this.processaAcaoLanterna();

	}

    @Override
    protected void onResume() {
        super.onResume();
    }

	@Override
	protected void onRestart() {
		super.onRestart();

		if (! getMarca().equalsIgnoreCase("motorola")) {

			this.prepara();

		} else {
			this.temFlash = this.temFlash();

			if (this.temFlash) {
				try {
					led = new DroidLED();
				} catch (Exception e) {
					Toast.makeText(this, "Deu pau ao instanciar LED Motorola.", Toast.LENGTH_SHORT).show();
				}
			}
		}

	}

    @Override
    protected void onPause(){
        super.onPause();

    }

    @Override
    protected void onStop() {
        super.onStop();

	    this.shutdown();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();

	    this.shutdown();

    }

	/** ********************************************************************************************/
	/**IMPLEMENTAÇÃO DA INTERFACE SurfaceHolder.Callback - NECESSARIA PARA O PREVIEW FUNCIONAR
	 * CORRETAMENTE E A CAMERA LIGAR O FLASH EM ALGUNS APARELHOS COMO SANSUNG ACE*/
	public void surfaceCreated(SurfaceHolder holder) {
		// APÓS CRIAR A SURFACE, SETAR NA CÂMERA QUAL A SAÍDA PARA O PREVIEW
		mHolder = holder;
		try {
			mCamera.setPreviewDisplay(mHolder);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		this.mCamera.stopPreview();
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {

	}
	/***********************************************************************************************/
    /**EVENTOS*/

    public void onClick_OnOff(View v) {

		if (!this.isCameraOn) {
			this.prepara();
		}

	    this.processaAcaoLanterna();

	}

	public void onClick_ivSom(View v) {
		ImageView ivSom = (ImageView) findViewById(R.id.ivSom);
		//-------------------
		SharedPreferences config = getPreferences(MODE_PRIVATE);
		SharedPreferences.Editor editor = config.edit();
		//-------------------
		if (som) {
			som = false;
			editor.putBoolean("som", som);
			ivSom.setImageDrawable(getResources().getDrawable(R.drawable.soundoff96x96));
		} else {
			som = true;
			editor.putBoolean("som", som);
			ivSom.setImageDrawable(getResources().getDrawable(R.drawable.soundon96x96));
		}

		editor.commit();

	}

	/** ****************************************************************************************** */
	/** MÉTODOS DA CLASSE*/

	private void processaAcaoLanterna() {

		ImageView mimageView = (ImageView) findViewById(R.id.imageView);

		if (getMarca().equalsIgnoreCase("motorola") && this.temFlash) {
			if (led.isEnabled()) {
				acesa = false;
				mimageView.setImageDrawable(getResources().getDrawable(R.drawable.lampada_apagada));
				if (som) {mpDesliga.start();}
			} else {
				acesa = true;
				mimageView.setImageDrawable(getResources().getDrawable(R.drawable.lampada_acesa));
				if (som) {mpLiga.start();}
			}

			led.enable(!led.isEnabled());

		//} else if (getMarca().equalsIgnoreCase("samsung")) {

		} else {
			if (this.temFlash) {

				if (!this.isCameraOn) {
					this.prepara();
				}

				if (!this.isFlashOn) {
					// LIGA O FLASH
					if (this.ligaFlash()) {
						//TOCA O SOM DE LIGAR
						if (som) {mpLiga.start();}

						// ALTERA A IMAGEM PARA A LAMPADA ACESA
						//TODO - TRANSFORMAR EM ANIMAÇÃO A TRANSIÇÃO DA IMAGEM APAGADA PARA ACESA;
						mimageView.setImageDrawable(getResources().getDrawable(R.drawable.lampada_acesa));

					} else {
						Toast.makeText(actLanterna.this, R.string.textNaoLigouFlash, Toast.LENGTH_LONG).show();
					}
				} else {
					//TOCA SOM DE DESLIGAR
					if (som) {mpDesliga.start();}

					this.desligaFlash();

					// ALTERA A IMAGEM PARA A LAMPADA APAGADA
                    //TODO - E DE ACESA PRA APAGADA;
					mimageView.setImageDrawable(getResources().getDrawable(R.drawable.lampada_apagada));
					//** ****************************************
				}
			} else {
				/**
				if(acesa){
					mimageView.setImageDrawable(getResources().getDrawable(R.drawable.lampada_apagada));
					acesa = false;
			        Toast.makeText(actLanterna.this, Build.MANUFACTURER, Toast.LENGTH_LONG).show();
				} else {
					mimageView.setImageDrawable(getResources().getDrawable(R.drawable.lampada_acesa));
					acesa = true;
				}
				*/
				//mpLiga.start();
				Intent i = new Intent(this, actTelaBranca.class);
				startActivity(i);
			}
		}

	}

	private boolean temCamera() {

		return getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);

	}

	private boolean temFlash() {
		return getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
	}

	private void liberaCamera() {

		if (this.mCamera!=null) {
			this.mCamera.release();
		}

		this.mCamera = null;
		this.isCameraOn = false;
		this.isFlashOn = false;

	}

	private void shutdown() {
		/**if (isFlashOn) {
			this.desligaFlash();
		}  */
		if (getMarca().equalsIgnoreCase("MOTOROLA") && this.temFlash) {
			if (led.isEnabled()) {
				led.enable(false);
				led = null;
			}
		}

		if (this.isCameraOn || this.mCamera != null) {
		 this.liberaCamera();
		}

		if (mpLiga != null) {
			mpLiga.release();
			mpLiga = null;

		}

		if (mpDesliga != null) {
			mpDesliga.release();
			mpDesliga = null;

		}


	}

	private void prepara() {

		if (mSurface == null) {
			mSurface = (SurfaceView) findViewById(R.id.surfaceView);
		}
		if (mHolder == null) {
			mHolder = mSurface.getHolder();
			mHolder.addCallback(this);
		}

		/** ************************************************************************* */
		this.temCamera = this.temCamera();

		if (this.temCamera) {
			this.isCameraOn = this.ligaCamera();

			this.temFlash = this.temFlash();

			if (this.isCameraOn) {

				try {
					mCamera.setPreviewDisplay(mHolder);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private boolean ligaFlash() {

		if (this.temFlash) {

			try {

				params = mCamera.getParameters();
				params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);

				this.mCamera.setParameters(params);

				this.mCamera.startPreview();

				isFlashOn = true;

				return true;

			} catch (RuntimeException e) {
				Log.d(TAG, "Erro rodando o starPreview" + e.getMessage());
				return false;
			}

		} else {

			Log.i(TAG, "Não rodou o ligaFlash()");

			return false;
		}
	}

	private void desligaFlash() {
		if (this.isFlashOn) {

			try {
				params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);

				this.mCamera.setParameters(params);

				this.mCamera.stopPreview();

				isFlashOn = false;

			} catch (RuntimeException e) {
				Log.d(TAG, "Erro rodando o starPreview" + e.getMessage());
			}
		}
	}

	private boolean ligaCamera(){

		if (! isCameraOn) {

			try {
				this.mCamera = Camera.open();
				return true;
			} catch (RuntimeException e) {
				Log.i(TAG, "Falha ao tentar instanciar a câmera: " + e.getMessage());
				return false;
			}
		} else {
			return true;

		}
	}

	private String getMarca() {
		return Build.MANUFACTURER;
	}
	/***********************************************************************************************/
}
